package pers.zh.kotlin.practice.exception

import com.fasterxml.jackson.annotation.JsonIgnore


class LogException (override val message: String) : RuntimeException(){
    @JsonIgnore
    override fun getStackTrace(): Array<StackTraceElement> {
        return super.getStackTrace()
    }
}