package pers.zh.kotlin.practice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import pers.zh.kotlin.practice.service.LogService
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/log")
class LogController {
    @Autowired
    @Qualifier("logService")
    private val logService: LogService? = null

    /**
     * 查看所有人日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param type 类型
     * @param nm 姓名
     * @param dt 时间
     * @param _page 页数
     * @param _pageSize 每页的大小
     * @return 当前页数据
     */
    @RequestMapping("/queryLogPage.do")
    @ResponseBody
    @Throws(Exception::class)
    fun queryLogPage(request: HttpServletRequest, nm: String, type: String, dt: String, _page: String, _pageSize: String): Map<String, Any> {
        val uid = request.session.getAttribute("uid").toString()
        val userLx = request.session.getAttribute("type").toString()
        return logService!!.queryLogPage(type, nm, dt, _page, _pageSize, uid, userLx)
    }

    /**
     * 查看个人日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param type 类型
     * @param dt 时间
     * @param _page 页数
     * @param _pageSize 每页的大小
     * @return 当前页数据
     */
    @RequestMapping("/queryMyLogPage.do")
    @ResponseBody
    @Throws(Exception::class)
    fun queryMyLogPage(request: HttpServletRequest, type: String, dt: String, _page: String, _pageSize: String): Map<String, Any> {
        val uid = request.session.getAttribute("uid").toString()
        return logService!!.queryLogPage(type, "", dt, _page, _pageSize, uid, "")
    }


    /**
     * 通过日志id 查看一条日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param id 日志id
     * @return 此id对应的数据
     */
    @RequestMapping("/queryLogById.do")
    @ResponseBody
    fun queryLogById(id: String): List<Map<String, Any>> {
        return logService!!.queryLogById(id)
    }


    /**
     * 所有日志操作类型的回显
     * 创建时间：2017-05-20
     * 创建人;赵辉
     * @param request
     * @return
     */
    @RequestMapping("/getLx.do")
    @ResponseBody
    fun getLX(request: HttpServletRequest): List<Map<String, Any>> {
        val uid = request.session.getAttribute("uid").toString()
        val userLx = request.session.getAttribute("type").toString()
        return logService!!.queryLx(uid, userLx)
    }

    /**
     * 个人日志操作类型的回显
     * 创建时间：2017-05-20
     * 创建人;赵辉
     * @param request
     * @return
     */
    @RequestMapping("/getMyLx.do")
    @ResponseBody
    fun getMLX(request: HttpServletRequest): List<Map<String, Any>> {
        val uid = request.session.getAttribute("uid").toString()
        return logService!!.queryMyLx(uid)
    }
}