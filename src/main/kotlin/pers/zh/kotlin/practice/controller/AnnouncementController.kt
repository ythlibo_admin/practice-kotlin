package pers.zh.kotlin.practice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.util.Assert
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import pers.zh.kotlin.practice.exception.GonggaoException
import pers.zh.kotlin.practice.service.AnnouncementService
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/gongGao")
class AnnouncementController {
    @Autowired
    @Qualifier("announcementService")
    private val announcementService: AnnouncementService? = null

    /**
     * 公告列表分页查询
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @param _page
     * @param _pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping("/queryGongGaoPage.do")
    @ResponseBody
    @Throws(Exception::class)
    fun queryGongGaoPage(request: HttpServletRequest, _page: String, _pageSize: String, cnm: String, vdt: String, bt: String): Map<String, Any> {
        return announcementService!!.queryGongGaoPage(request, _page, _pageSize, bt, cnm, vdt)

    }


    /**
     * 根据公告ID进行删除
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @param nid
     * @return
     * @throws Exception
     */
    @RequestMapping("/delByNid.do")
    @ResponseBody
    @Throws(Exception::class)
    fun delByNid(request: HttpServletRequest,
                 nid: String): Map<String, Any> {
        return announcementService!!.delByNid(request, nid)
    }


    /**
     * 根据公告ID进行查询
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @param nid 公告ID
     * @return
     * @throws Exception
     */
    @RequestMapping("/gongGaoByNid.do")
    @ResponseBody
    @Throws(Exception::class)
    fun gongGaoByNid(request: HttpServletRequest,
                     nid: String): Map<String, Any> {
        val uid = request.session.getAttribute("uid").toString()
        return announcementService!!.doReadGongGaoByNid(nid, uid)
    }

    //发公告
    @RequestMapping("/sendGongGao.do")
    @ResponseBody
    @Throws(Exception::class)
    fun sendMes(request: HttpServletRequest,
                bt: String, nr: String): Map<String, Any> {
        Assert.isTrue(!StringUtils.isEmpty(bt), "标题不能为空")
        Assert.isTrue(!StringUtils.isEmpty(nr), "内容不能为空")
        return announcementService!!.doSendGongGao(request, bt, nr)
    }


    //查看是否有发公告的权限
    @RequestMapping("/queryButton.do")
    @ResponseBody
    @Throws(Exception::class)
    fun queryButton(request: HttpServletRequest): Map<String, Any> {
        return announcementService!!.queryButton(request)
    }


    //处理异常
    @ExceptionHandler(GonggaoException::class)
    fun gongGaoException(ee: GonggaoException): ResponseEntity<*> {
        return ResponseEntity<Any>(ee, HttpStatus.BAD_REQUEST)
    }

}