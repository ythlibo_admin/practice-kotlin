package pers.zh.kotlin.practice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pers.zh.kotlin.practice.service.TestService

/**
 * 测试
 */
@RestController
@RequestMapping("/test")
class TestController {
    @Autowired
    private val testService: TestService? = null

    /**
     * 分页查询测试
     * @return
     * @throws Exception
     */
    @RequestMapping("/getAll")
    @Throws(Exception::class)
    fun getAll(): Map<String, Any> {
        return testService!!.queryAll()
    }
}