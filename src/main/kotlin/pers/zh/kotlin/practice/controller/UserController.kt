package pers.zh.kotlin.practice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.Assert
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*
import pers.zh.kotlin.practice.exception.UserException
import pers.zh.kotlin.practice.service.UserService
import javax.servlet.http.HttpServletRequest

/**
 * 用户 Controller
 * @author Zhao Hui
 */
@RestController
@RequestMapping("/user")
class UserController {
    @Autowired
    private val userService: UserService? = null

    //分页查询，所有用户信息
    @PostMapping("/queryPage.do")
    @ResponseBody
    @Throws(Exception::class)
    fun queryPage(request: HttpServletRequest, _page: String, _pageSize: String, sex: String, nm: String, type: String, del: String): Map<String, Any> {
        Assert.isTrue(!StringUtils.isEmpty(_page), "page参数错误")
        Assert.isTrue(!StringUtils.isEmpty(_pageSize), "pageSize参数错误")
        val douid = request.session.getAttribute("uid").toString()

        return userService!!.queryPage(douid, _page, _pageSize, sex, nm, type, del)
    }


    //添加中心用户

    /**
     * @param user
     * @param pas
     * @param nm
     */
    @PostMapping("/addUser.do")
    @ResponseBody
    @Throws(Exception::class)
    fun addUser(request: HttpServletRequest, user: String, pas: String, nm: String, sex: String, birthday: String, type: String, del: String): Map<String, Any> {
        Assert.isTrue(!StringUtils.isEmpty(user), "用户名不能为空")
        Assert.isTrue(!StringUtils.isEmpty(pas), "密码不能为空")
        Assert.isTrue(!StringUtils.isEmpty(nm), "姓名不能为空")
        Assert.isTrue(!StringUtils.isEmpty(type), "类型不能为空")
        Assert.isTrue(!StringUtils.isEmpty(sex), "性别不能为空")
        Assert.isTrue(!StringUtils.isEmpty(birthday), "生日不能为空")
        Assert.isTrue(!StringUtils.isEmpty(del), "有效性不能为空")

        val douid = request.session.getAttribute("uid").toString()

        return userService!!.addUser(douid, user, pas, nm, sex, birthday, type, del)

    }


    /**
     * 根据UID查看用户的信息
     * @param uid
     * @return
     */
    @PostMapping("/queryUserByUid.do")
    @ResponseBody
    fun queryUser(uid: String): Map<String, Any> {

        Assert.isTrue(!StringUtils.isEmpty(uid), "uid不能为空")

        return userService!!.queryUserByUid(uid)
    }

    @PostMapping("/editUserByUid.do")
    @ResponseBody
    @Throws(Exception::class)
    fun editUserByUid(request: HttpServletRequest, uid: String, type: String, del: String): Map<String, Any> {

        Assert.isTrue(!StringUtils.isEmpty(uid), "uid不能为空")
        Assert.isTrue(!StringUtils.isEmpty(type), "type不能为空")
        Assert.isTrue(!StringUtils.isEmpty(del), "del不能为空")

        val douid = request.session.getAttribute("uid").toString()

        return userService!!.doEditUserByUid(douid, uid, type, del)
    }

    /**
     * 重置用户密码
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @param uid
     * @return
     * @throws Exception
     */
    @RequestMapping("/doRePas.do")
    @ResponseBody
    @Throws(Exception::class)
    fun doRePas(request: HttpServletRequest,
                uid: String): Map<String, Any> {
        return userService!!.doRePas(request, uid)
    }


    /**
     * 获取服务器时间
     * @return time
     */
    @PostMapping("/serviceTime.do")
    fun serviceTime(): Long {
        return System.currentTimeMillis()
    }

    /**
     * 获取用户名
     * @return queryName
     */
    @PostMapping("/queryName.do")
    @Throws(Exception::class)
    fun queryName(request: HttpServletRequest): Map<String, String> {
        return userService!!.queryName(request)
    }

    /**
     * 获取生日信息
     * @return queryBir
     */
    @PostMapping("/queryBir.do")
    @Throws(Exception::class)
    fun queryBir(): Map<String, Any> {
        return userService!!.queryBir()
    }

    /**
     * 查询登录人的权限
     * @return queryPop
     */
    @PostMapping("/queryPop.do")
    @Throws(Exception::class)
    fun queryPop(request: HttpServletRequest): Map<String, Any> {
        return userService!!.queryPop(request)
    }

    /**
     * 查询权限ByUid
     * @return queryPop
     */
    @PostMapping("/queryPopByUid.do")
    @Throws(Exception::class)
    fun queryPopByUid(uid: String): Map<String, Any> {
        return userService!!.queryPopByUid(uid)
    }


    @RequestMapping("/editPopByUid.do")
    @ResponseBody
    @Throws(Exception::class)
    fun editPopByUid(request: HttpServletRequest,
                     uid: String, pops: Array<String>): Map<String, Any> {
        Assert.isTrue(!StringUtils.isEmpty(uid), "被修改人不能为空")
        return userService!!.editPopByUid(request, uid, pops)
    }

    //处理异常
    @ExceptionHandler(UserException::class)
    fun userException(ee: UserException): ResponseEntity<*> {
        return ResponseEntity<Any>(ee, HttpStatus.BAD_REQUEST)
    }

}