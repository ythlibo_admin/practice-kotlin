package pers.zh.kotlin.practice.service

import javax.servlet.http.HttpServletRequest


interface LoginService {
    //登录
    abstract fun login(user: String, pas: String, request: HttpServletRequest): Boolean?
}