package pers.zh.kotlin.practice.service.impl

import com.google.common.base.Charsets
import com.google.common.hash.Hashing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pers.zh.kotlin.practice.exception.UserLoginFailedException
import pers.zh.kotlin.practice.mapper.LoginMapper
import pers.zh.kotlin.practice.service.LogService
import pers.zh.kotlin.practice.service.LoginService
import java.util.ArrayList
import javax.servlet.http.HttpServletRequest

@Service("loginService")
class LoginServiceImpl : LoginService{
    @Autowired
    private val loginMapper: LoginMapper? = null

    @Autowired
    private val logService: LogService? = null

    @Transactional
    override fun login(user: String, pas: String, request: HttpServletRequest): Boolean? {
        var list: List<Map<String, Any>> = ArrayList()
        try {
            list = loginMapper!!.queryUser(user)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (list.isEmpty()) {
            throw UserLoginFailedException("用户不存在")
        }

        val relPas = list[0]["PAS"].toString()
        val uid = list[0]["UID"].toString()
        val del = Integer.parseInt(list[0]["DEL"].toString())
        val type = Integer.parseInt(list[0]["TYPE"].toString())

        if (relPas != Hashing.md5().hashString(pas, Charsets.UTF_8).toString()) {
            throw UserLoginFailedException("密码错误")
        }
        if (del != 1) {
            throw UserLoginFailedException("账号已停用")
        }
        val session = request.session
        session.setAttribute("uid", uid)
        session.setAttribute("type", type)

        logService!!.addLog("登录", uid + "进行了登陆")

        return true
    }

}