package pers.zh.kotlin.practice.service

interface LogService {
    /**
     * 功能:插入日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param lx 操作类型
     * @param nr 操作内容
     * @return true false
     */
    abstract fun addLog(lx: String, nr: String): Boolean?

    /**
     * 查看日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param type 类型
     * @param nm 姓名
     * @param dt 时间
     * @param page 页数
     * @param pageSize 每页的大小
     * @return 当前页数据
     */
    @Throws(Exception::class)
    abstract fun queryLogPage(type: String, nm: String, dt: String, page: String, pageSize: String, uid: String, userLx: String): Map<String, Any>

    /**
     * 通过日志id 查看一条日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param id 日志id
     * @return 此id对应的数据
     */
    abstract fun queryLogById(id: String): List<Map<String, Any>>

    /**
     * 所有日志操作类型的回显
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param uid
     * @param userLx
     * @return
     */
    abstract fun queryLx(uid: String, userLx: String): List<Map<String, Any>>

    /**
     * 个人日志操作类型的回显
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param uid
     * @return
     */
    abstract fun queryMyLx(uid: String): List<Map<String, Any>>
}