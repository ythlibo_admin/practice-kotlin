package pers.zh.kotlin.practice.service.impl

import com.github.pagehelper.PageHelper
import com.github.pagehelper.PageInfo
import com.google.common.base.Charsets
import com.google.common.hash.Hashing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pers.zh.kotlin.practice.exception.NoPermissioneException
import pers.zh.kotlin.practice.exception.SqlException
import pers.zh.kotlin.practice.exception.UserException
import pers.zh.kotlin.practice.mapper.UserMapper
import pers.zh.kotlin.practice.service.LogService
import pers.zh.kotlin.practice.service.UserService
import pers.zh.kotlin.practice.util.MyUtil
import java.util.HashMap
import javax.servlet.http.HttpServletRequest

@Service("userService")
class UserServiceImpl : UserService {
    @Autowired
    private val userMapper: UserMapper? = null

    @Autowired
    private val logService: LogService? = null

    private val md5 = Hashing.md5()

    //分页查询
    @Throws(Exception::class)
    override fun queryPage(douid: String, _page: String, _pageSize: String, sex: String, nm: String, type: String, del: String): Map<String, Any> {

        val result = HashMap<String, Any>()

        if (!hasPop(douid, "1")) {
            throw NoPermissioneException("无操作权限")
        }

        //分页
        PageHelper.startPage<Any>(Integer.valueOf(_page)!!, Integer.valueOf(_pageSize)!!)
        val mapList = userMapper!!.selectPage(del, nm, type, sex)
        result.put("data", mapList)
        //总数
        val page = PageInfo<Map<String, Any>>(mapList)
        result.put("count", page.total)

        return result

    }


    /**
     * 管理员添加用户
     */
    @Transactional
    @Throws(Exception::class)
    override fun addUser(douid: String, user: String, pas: String, nm: String, sex: String, birthday: String, type: String, del: String): Map<String, Any> {
        //		定义返回值
        val map = HashMap<String, Any>()

        val dt = MyUtil.getDatetime()
        val ip = MyUtil.getIpAddr()
        val uid = MyUtil.newGuid()

        if (queryIfHasUser(user)) {
            throw UserException("用户名重复")
        }

        if (!hasPop(douid, "1")) {
            throw NoPermissioneException("无操作权限")
        }

        //		添加
        if (userMapper!!.insertUser(uid, user, pas, nm, sex, birthday, type, douid, dt, ip!!, del)!! > 0) {
            map.put("result", true)
            map.put("message", "添加成功！")
            logService!!.addLog("添加", douid + "添加用户：" + uid)
        } else {
            throw SqlException("数据超时，请刷新后重试！")
        }

        return map
    }


    /**
     * 修改用户信息
     */
    @Transactional
    @Throws(Exception::class)
    override fun doEditUserByUid(douid: String, uid: String, type: String, del: String): Map<String, Any> {
        //		定义返回值
        val map = HashMap<String, Any>()

        val upUid = douid
        val upDt = MyUtil.getDatetime()
        val upIp = MyUtil.getIpAddr()

        if (!hasPop(douid, "1")) {
            throw NoPermissioneException("无操作权限")
        }
        //		修改
        if (userMapper!!.updateUser(uid, upUid, upDt, upIp!!, type, del)!! > 0) {
            map.put("result", true)
            map.put("message", "修改成功！")
            logService!!.addLog("修改", douid + "修改了用户：" + uid + "的信息")

        } else {
            throw SqlException("数据超时，请刷新后重试！")
        }
        return map
    }

    /**
     * 根据UID查看用户的所有信息
     * @param uid
     * @return
     */
    override fun queryUserByUid(uid: String): Map<String, Any> {
        return userMapper!!.queryUserByUid(uid)
    }

    /**
     * 查询用户名
     * @param request
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    override fun queryName(request: HttpServletRequest): Map<String, String> {
        val map = HashMap<String, String>()
        val uid = request.session.getAttribute("uid").toString()
        val queryUserByUid = userMapper!!.queryUserByUid(uid)
        if (queryUserByUid.isNotEmpty()) {
            map.put("name", queryUserByUid.get("NM").toString())
        } else {
            map.put("name", "无")
        }
        return map

    }


    /**
     * 查询生日信息
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    override fun queryBir(): Map<String, Any> {
        val map = HashMap<String, Any>()
        val count = userMapper!!.queryBirCount()
        if (count != null) {
            if (count > 0) {
                map.put("result", true)
                map.put("data", userMapper!!.queryBirUser())
                map.put("count", count)
            } else {
                map.put("result", false)
            }
        }
        return map

    }


    //重置用户密码
    @Transactional
    @Throws(Exception::class)
    override fun doRePas(request: HttpServletRequest, uid: String): Map<String, Any> {
        //		定义返回值
        val map = HashMap<String, Any>()
        val pas = md5.hashString("1", Charsets.UTF_8).toString()
        val upuid = request.session.getAttribute("uid").toString()
        val upip = MyUtil.getIpAddr()
        val updt = MyUtil.getDatetime()
        //		判断参数和权限
        if (!hasPop(upuid, "1")) {
            throw NoPermissioneException("无操作权限")
        }
        //		更新
        if (userMapper!!.doRePas(uid, pas, upuid, updt, upip!!)!! > 0) {
            map.put("result", true)
            map.put("message", "重置成功！")
            logService!!.addLog("修改", upuid + "重置用户：" + uid + "的密码为：" + pas)
        } else {
            throw SqlException("数据超时，请刷新后重试！")
        }


        return map
    }


    /**
     * 查询登录人的权限
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    override fun queryPop(request: HttpServletRequest): Map<String, Any> {
        val map = HashMap<String, Any>()
        val uid = request.session.getAttribute("uid").toString()
        map.put("data", userMapper!!.queryPop(uid))
        return map

    }

    /**
     * 查询权限ByUid
     * @return
     * @throws Exception
     */

    @Throws(Exception::class)
    override fun queryPopByUid(uid: String): Map<String, Any> {
        val map = HashMap<String, Any>()
        map.put("data", userMapper!!.queryPop(uid))
        return map

    }


    /**
     * 修改权限
     * @param request
     * @param uid
     * @param pops
     * @return
     * @throws Exception
     */
    @Transactional
    @Throws(Exception::class)
    override fun editPopByUid(request: HttpServletRequest, uid: String, pops: Array<String>): Map<String, Any> {
        val map = HashMap<String, Any>()
        val douid = request.session.getAttribute("uid").toString()
        var id : String
        try {
            if (!hasPop(douid, "1")) {
                throw NoPermissioneException("无操作权限")
            }

            if (pops.isNotEmpty()) {
                userMapper!!.delPopByUid(uid)
                for (pid in pops) {
                    id = MyUtil.newGuid()
                    userMapper!!.editPopByUid(id, uid, pid)
                }
                logService!!.addLog("修改", douid + "修改了" + uid + "的权限")
            } else {
                userMapper!!.delPopByUid(uid)
                logService!!.addLog("修改", douid + "修改了" + uid + "的权限")
            }
            map.put("result", true)
            map.put("message", "修改成功")

            return map
        } catch (e: Exception) {
            //数据库异常时的操作
            throw SqlException("数据超时，请刷新后重试！")

        }

    }

    //查询是否有此权限
    @Throws(Exception::class)
    override fun hasPop(uid: String, pid: String): Boolean {
        return userMapper!!.checkPop(uid, pid)!! > 0
    }

    /**
     * 查询用户名是否存在
     * @param user
     * @return
     */
    private fun queryIfHasUser(user: String): Boolean {
        return userMapper!!.queryUser(user).isNotEmpty()
    }
}