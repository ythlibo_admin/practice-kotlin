package pers.zh.kotlin.practice.service

interface TestService {
    /**
     * 分页查询测试
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun queryAll(): Map<String, Any>
}