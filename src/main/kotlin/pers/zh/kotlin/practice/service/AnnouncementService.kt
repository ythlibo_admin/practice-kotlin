package pers.zh.kotlin.practice.service

import javax.servlet.http.HttpServletRequest

interface AnnouncementService {
    /**
     * 公告列表分页查询
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @param _page
     * @param _pageSize
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun queryGongGaoPage(request: HttpServletRequest, _page: String, _pageSize: String, bt: String, cnm: String, vdt: String): Map<String, Any>


    /**
     * 根据NID进行删除
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @param nid
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun delByNid(request: HttpServletRequest, nid: String): Map<String, Any>


    /**
     * 根据NID进行查询
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param nid 公告ID
     * @return
     * @throws Exception
     */
    abstract fun doReadGongGaoByNid(nid: String, uid: String): Map<String, Any>

    /**
     * 发公告
     * @param request
     * @param bt
     * @param nr
     * @return
     * @throws Exception
     */
    abstract fun doSendGongGao(request: HttpServletRequest, bt: String, nr: String): Map<String, Any>

    /**
     * 发公告
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @return
     */
    abstract fun queryButton(request: HttpServletRequest): Map<String, Any>

}