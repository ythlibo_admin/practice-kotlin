package pers.zh.kotlin.practice.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pers.zh.kotlin.practice.exception.NoPermissioneException
import pers.zh.kotlin.practice.mapper.TongJiMapper
import pers.zh.kotlin.practice.service.TongJiService
import pers.zh.kotlin.practice.service.UserService
import java.util.ArrayList
import java.util.HashMap
import javax.servlet.http.HttpServletRequest

@Service("tongJiService")
class TongJiServiceImpl : TongJiService {
    @Autowired
    private val tongJiMapper: TongJiMapper? = null

    @Autowired
    private val userService: UserService? = null

    //生日统计
    @Throws(Exception::class)
    override fun queryBir(request: HttpServletRequest): Map<String, Any> {
        val map = HashMap<String, Any>()
        val months = ArrayList<String>()
        val sum = ArrayList<Int>()
        val uid = request.session.getAttribute("uid").toString()
        months.add("1月")
        months.add("2月")
        months.add("3月")
        months.add("4月")
        months.add("5月")
        months.add("6月")
        months.add("7月")
        months.add("8月")
        months.add("9月")
        months.add("10月")
        months.add("11月")
        months.add("12月")

        if (!userService!!.hasPop(uid, "2")) {
            throw NoPermissioneException("无操作权限")
        }

        val list = tongJiMapper!!.queryBir()
        for (i in 1..12) {
            for (data in list) {
                if (i == Integer.valueOf(data.get("DT").toString())) {
                    sum.add(Integer.valueOf(data.get("SUM").toString()))
                }
            }
            if (sum.size < i) {
                sum.add(0)
            }
        }

        map.put("months", months)
        map.put("sum", sum)
        return map
    }


    @Throws(Exception::class)
    override fun querySex(request: HttpServletRequest): Map<String, Any> {
        val map = HashMap<String, Any>()
        val sex = ArrayList<String>()
        var querySex: List<Map<String, Any>> = ArrayList()

        val uid = request.session.getAttribute("uid").toString()
        if (!userService!!.hasPop(uid, "2")) {
            throw NoPermissioneException("无操作权限")
        }

        querySex = tongJiMapper!!.querySex()

        for (a in querySex) {
            sex.add(a["name"].toString())
        }

        map.put("count", querySex)
        map.put("sex", sex)
        return map
    }
}