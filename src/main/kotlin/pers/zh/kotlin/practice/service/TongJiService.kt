package pers.zh.kotlin.practice.service

import javax.servlet.http.HttpServletRequest

interface TongJiService {
    @Throws(Exception::class)
    abstract fun queryBir(request: HttpServletRequest): Map<String, Any>


    /**
     * 性别统计
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun querySex(request: HttpServletRequest): Map<String, Any>
}