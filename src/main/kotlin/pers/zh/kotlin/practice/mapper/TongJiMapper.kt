package pers.zh.kotlin.practice.mapper


interface TongJiMapper {
    /**
     * 生日统计
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun queryBir(): List<Map<String, Any>>

    /**
     * 性别统计
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun querySex(): List<Map<String, Any>>
}