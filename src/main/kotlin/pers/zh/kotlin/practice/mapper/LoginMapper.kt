package pers.zh.kotlin.practice.mapper


interface LoginMapper {
    abstract fun queryUser(user: String): List<Map<String, Any>>
}