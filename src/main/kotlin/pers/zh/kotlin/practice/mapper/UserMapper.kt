package pers.zh.kotlin.practice.mapper

import org.apache.ibatis.annotations.Param

interface UserMapper {
    //分页查询
    @Throws(Exception::class)
    abstract fun selectPage(@Param("del") del: String, @Param("nm") nm: String, @Param("type") type: String, @Param("sex") sex: String): List<Map<String, Any>>

    //添加用户
    @Throws(Exception::class)
    abstract fun insertUser(@Param("uid") uid: String, @Param("user") user: String, @Param("pas") pas: String, @Param("nm") nm: String, @Param("sex") sex: String, @Param("birthday") birthday: String, @Param("type") type: String, @Param("douid") douid: String, @Param("dt") dt: String, @Param("ip") ip: String, @Param("del") del: String): Int?

    //通过UID查询用户的所有信息
    abstract fun queryUserByUid(@Param("uid") uid: String): Map<String, Any>

    //修改用户信息
    abstract fun updateUser(@Param("uid") uid: String, @Param("upUid") upUid: String, @Param("upDt") upDt: String, @Param("upIp") upIp: String, @Param("type") type: String, @Param("del") del: String): Int?


    //查询当天生日的人
    @Throws(Exception::class)
    abstract fun queryBirUser(): List<Map<String, Any>>

    //查询当天生日的总数
    @Throws(Exception::class)
    abstract fun queryBirCount(): Int?

    //重置密码
    @Throws(Exception::class)
    abstract fun doRePas(@Param("uid") uid: String, @Param("pas") pas: String, @Param("upuid") upuid: String, @Param("updt") updt: String, @Param("upip") upip: String): Int?

    //查询登录人的权限
    @Throws(Exception::class)
    abstract fun queryPop(@Param("uid") uid: String): List<Map<String, Any>>

    //修改权限
    @Throws(Exception::class)
    abstract fun editPopByUid(@Param("id") id: String, @Param("uid") uid: String, @Param("pid") pid: String): Int?

    //删除权限
    @Throws(Exception::class)
    abstract fun delPopByUid(@Param("uid") uid: String): Int?

    //查询是否有此权限
    @Throws(Exception::class)
    abstract fun checkPop(@Param("uid") uid: String, @Param("pid") pid: String): Int?

    //查看此用户名是否存在
    abstract fun queryUser(@Param("user") user: String): List<Map<String, Any>>

}